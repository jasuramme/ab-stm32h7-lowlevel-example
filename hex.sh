#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

arm-none-eabi-objcopy -O ihex $DIR/build/epath2 $DIR/build/epath2.hex
arm-none-eabi-objcopy -O binary $DIR/build/epath2 $DIR/build/epath2.bin
