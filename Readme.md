# Aina-Bulaque STM32H723 board example code

This repository contains example project for open source board.
The only features are:
- Ethernet HAL driven layer 2 transmitter
- SDRAM W9825G6KH related code.

## How to build it?

You need CMake, arm-none-eabi-gcc. And run configure.sh. As soon as it is CMake project, you can use any IDE which does support CMake properly.

## Related repos:

- LWIP full TCP-IP stack example: [https://gitlab.com/jasuramme/custom_stmh723_eth](https://gitlab.com/jasuramme/custom_stmh723_eth)
- Open Hardware board design: [https://gitlab.com/jasuramme/ab-stm32h723](https://gitlab.com/jasuramme/ab-stm32h723)