#pragma once

#include <stdint.h>

#define TEST_TYPE uint64_t

uint32_t memtest(uint32_t addrStart, uint32_t addrEnd);