#include "ethernet.h"
#include "lan8742.h"
#include "main.h"
#include "stm32h7xx_hal.h"
#include <stdint.h>
#include <string.h>

#define ETH_RX_BUFFER_SIZE 1536
#define ETH_TX_DESC_CNT 4U /* number of Ethernet Tx DMA descriptors */
#define ETH_RX_DESC_CNT 4U /* number of Ethernet Rx DMA descriptors */

ETH_HandleTypeDef heth;
ETH_TxPacketConfig TxConfig;

typedef struct {
    uint8_t buff[(ETH_RX_BUFFER_SIZE + 31) & ~31] __ALIGNED(32);
} RxBuff_t;

static void init_mempool() {}
ETH_DMADescTypeDef DMARxDscrTab[ETH_RX_DESC_CNT] __attribute__((
    section(".RxDecripSection"))); /* Ethernet Rx DMA Descriptors */
ETH_DMADescTypeDef DMATxDscrTab[ETH_TX_DESC_CNT] __attribute__((
    section(".TxDecripSection"))); /* Ethernet Tx DMA Descriptors */
RxBuff_t RX_BUFFERS[ETH_RX_DESC_CNT]
    __attribute__((section(".Rx_PoolSection")));

int32_t ETH_PHY_IO_Init(void);
int32_t ETH_PHY_IO_DeInit(void);
int32_t ETH_PHY_IO_ReadReg(uint32_t DevAddr, uint32_t RegAddr,
                           uint32_t *pRegVal);
int32_t ETH_PHY_IO_WriteReg(uint32_t DevAddr, uint32_t RegAddr,
                            uint32_t RegVal);
int32_t ETH_PHY_IO_GetTick(void);

lan8742_Object_t LAN8742;
lan8742_IOCtx_t LAN8742_IOCtx = {ETH_PHY_IO_Init, ETH_PHY_IO_DeInit,
                                 ETH_PHY_IO_WriteReg, ETH_PHY_IO_ReadReg,
                                 ETH_PHY_IO_GetTick};

void HAL_ETH_RxLinkCallback(void **pStart, void **pEnd, uint8_t *buff,
                            uint16_t Length) {
    // Received packet processing here
    return;
}

void HAL_ETH_RxAllocateCallback(uint8_t **buff) {
    static int buffer = 0;
    *buff = (uint8_t *)(&RX_BUFFERS[buffer]);
    if (buffer++ > 4)
        buffer = 0;
    return;
}

/**
 * @brief ETH Initialization Function
 * @param None
 * @retval None
 */
void MX_ETH_Init(void) {
    static uint8_t MACAddr[6];
    heth.Instance = ETH;
    MACAddr[0] = 0x00;
    MACAddr[1] = 0x80;
    MACAddr[2] = 0xE1;
    MACAddr[3] = 0x00;
    MACAddr[4] = 0x00;
    MACAddr[5] = 0x00;
    heth.Init.MACAddr = &MACAddr[0];
    heth.Init.MediaInterface = HAL_ETH_RMII_MODE;
    heth.Init.TxDesc = DMATxDscrTab;
    heth.Init.RxDesc = DMARxDscrTab;
    heth.Init.RxBuffLen = 1536;

    if (HAL_ETH_Init(&heth) != HAL_OK) {
        Error_Handler();
    }

    memset(&TxConfig, 0, sizeof(ETH_TxPacketConfig));
    TxConfig.Attributes =
        ETH_TX_PACKETS_FEATURES_CSUM | ETH_TX_PACKETS_FEATURES_CRCPAD;
    TxConfig.ChecksumCtrl = ETH_CHECKSUM_IPHDR_PAYLOAD_INSERT_PHDR_CALC;
    TxConfig.CRCPadCtrl = ETH_CRC_PAD_INSERT;

    if (HAL_ETH_Start_IT(&heth) != HAL_OK) {
        Error_Handler();
    }

    ETH->DMACIER |= ETH_DMACIER_RIE;
}

void update_link_status() {
    ETH_MACConfigTypeDef MACConf = {0};
    LAN8742_RegisterBusIO(&LAN8742, &LAN8742_IOCtx);

    /* Initialize the LAN8742 ETH PHY */
    LAN8742_Init(&LAN8742);
    int32_t PHYLinkState = 0;
    PHYLinkState = LAN8742_GetLinkState(&LAN8742);
    uint32_t duplex, speed = 0;
    uint8_t link_down = 1;
    if (PHYLinkState <= LAN8742_STATUS_LINK_DOWN) {
        link_down = 1;
    } else {
        switch (PHYLinkState) {
        case LAN8742_STATUS_100MBITS_FULLDUPLEX:
            duplex = ETH_FULLDUPLEX_MODE;
            speed = ETH_SPEED_100M;
            break;
        case LAN8742_STATUS_100MBITS_HALFDUPLEX:
            duplex = ETH_HALFDUPLEX_MODE;
            speed = ETH_SPEED_100M;
            break;
        case LAN8742_STATUS_10MBITS_FULLDUPLEX:
            duplex = ETH_FULLDUPLEX_MODE;
            speed = ETH_SPEED_10M;
            break;
        case LAN8742_STATUS_10MBITS_HALFDUPLEX:
            duplex = ETH_HALFDUPLEX_MODE;
            speed = ETH_SPEED_10M;
            break;
        default:
            duplex = ETH_FULLDUPLEX_MODE;
            speed = ETH_SPEED_100M;
            break;
        }

        if (link_down) {
            link_down = 0;
            /* Get MAC Config MAC */
            HAL_ETH_GetMACConfig(&heth, &MACConf);
            MACConf.DuplexMode = duplex;
            MACConf.Speed = speed;
            HAL_ETH_SetMACConfig(&heth, &MACConf);

            HAL_ETH_Start_IT(&heth);
        }
    }
}

#define BUF_LEN 2000
void ETH_test_tx() {
    uint8_t buffer[BUF_LEN];
    for (int i = 0; i < BUF_LEN; ++i) {
        buffer[i] = (uint8_t)i;
    }
    int len = BUF_LEN;
    ETH_BufferTypeDef Txbuffer[1];
    Txbuffer[0].buffer = buffer;
    Txbuffer[0].len = len;
    Txbuffer[0].next = NULL;

    /* ETH packet  TxConfig is defined and initialized in eth.c*/
    TxConfig.Length = 1;
    TxConfig.TxBuffer = Txbuffer;
    HAL_StatusTypeDef ret = HAL_ETH_Transmit_IT(&heth, &TxConfig);
    static volatile int errors = 0;
    static volatile int attempts = 0;
    attempts++;
    if (ret != HAL_OK) {
        errors++;
    }
}

void HAL_ETH_RxCpltCallback(ETH_HandleTypeDef *handlerEth) {
    void *p = NULL;
    HAL_ETH_ReadData(handlerEth, (void **)&p);
}

/*******************************************************************************
                       PHI IO Functions
*******************************************************************************/
/**
 * @brief  Initializes the MDIO interface GPIO and clocks.
 * @param  None
 * @retval 0 if OK, -1 if ERROR
 */
int32_t ETH_PHY_IO_Init(void) {
    /* We assume that MDIO GPIO configuration is already done
       in the ETH_MspInit() else it should be done here
    */

    /* Configure the MDIO Clock */
    HAL_ETH_SetMDIOClockRange(&heth);

    return 0;
}

/**
 * @brief  De-Initializes the MDIO interface .
 * @param  None
 * @retval 0 if OK, -1 if ERROR
 */
int32_t ETH_PHY_IO_DeInit(void) { return 0; }

/**
 * @brief  Read a PHY register through the MDIO interface.
 * @param  DevAddr: PHY port address
 * @param  RegAddr: PHY register address
 * @param  pRegVal: pointer to hold the register value
 * @retval 0 if OK -1 if Error
 */
int32_t ETH_PHY_IO_ReadReg(uint32_t DevAddr, uint32_t RegAddr,
                           uint32_t *pRegVal) {
    volatile int pause = 0;
    if (HAL_ETH_ReadPHYRegister(&heth, DevAddr, RegAddr, pRegVal) != HAL_OK) {
        pause++;
        return -1;
    }
    volatile uint32_t test = pRegVal[0];
    pause++;
    return 0;
}

/**
 * @brief  Write a value to a PHY register through the MDIO interface.
 * @param  DevAddr: PHY port address
 * @param  RegAddr: PHY register address
 * @param  RegVal: Value to be written
 * @retval 0 if OK -1 if Error
 */
int32_t ETH_PHY_IO_WriteReg(uint32_t DevAddr, uint32_t RegAddr,
                            uint32_t RegVal) {
    if (HAL_ETH_WritePHYRegister(&heth, DevAddr, RegAddr, RegVal) != HAL_OK) {
        return -1;
    }

    return 0;
}

/**
 * @brief  Get the time in millisecons used for internal PHY driver process.
 * @retval Time value
 */
int32_t ETH_PHY_IO_GetTick(void) { return HAL_GetTick(); }