#include "memtest.h"

TEST_TYPE hash_gen(uint32_t addr) { return 0x12ab + addr; }

uint32_t memtest(uint32_t addrStart, uint32_t addrEnd) {

    for (int i = 0; i < addrEnd; i += sizeof(TEST_TYPE)) {
        *(volatile TEST_TYPE *)(addrStart + i) = hash_gen(i);
    }

    volatile int errors = 0;

    for (int i = 0; i < addrEnd; i += sizeof(TEST_TYPE)) {
        uint32_t addr = addrStart + i;
        TEST_TYPE val = hash_gen(i);
        TEST_TYPE realval = *(volatile TEST_TYPE *)(addr);
        if (realval != val)
            ++errors;
    }

    if (errors > 0) {
        return errors;
    }

    for (int i = 0; i < addrEnd; i += sizeof(TEST_TYPE)) {
        *(volatile TEST_TYPE *)(addrStart + i) = ~hash_gen(i);
    }

    for (int i = 0; i < addrEnd; i += sizeof(TEST_TYPE)) {
        uint32_t addr = addrStart + i;
        TEST_TYPE val = ~hash_gen(i);
        TEST_TYPE realval = *(volatile TEST_TYPE *)(addr);
        if (realval != val)
            ++errors;
    }

    return errors;
}
