/**
 ******************************************************************************
 * @file    stm32h747i_discovery_conf_template.h
 * @author  MCD Application Team
 * @brief   STM32H747I_DISCO board configuration file.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2018 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef STM32H747I_DISCO_CONF_H
#define STM32H747I_DISCO_CONF_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

/* COM define */
#define USE_COM_LOG 0U
#define USE_BSP_COM_FEATURE 0U

/* IRQ priorities */
#define BSP_SDRAM_IT_PRIORITY 15U

#ifdef __cplusplus
}
#endif

#endif /* STM32H747I_DISCO_CONF_H */
